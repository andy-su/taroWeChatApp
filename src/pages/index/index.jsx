import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { AtTabs, AtTabsPane, AtBadge } from 'taro-ui'
import { add, minus, asyncAdd } from '../../actions/counter'

import ff_icon_1 from '../../asset/yuedu.svg'
import ff_icon_2 from '../../asset/gongzhang.svg'
import ff_icon_3 from '../../asset/tianjia.svg'

import arrow_right from '../../asset/arrow/arrow_right.svg'
import mine_top from '../../asset/home_top_bg.png'

import device_smoke_detector from '../../asset/device/smoke_detector.png'
import device_temp_detector from '../../asset/device/temp_detector.png'
import device_water_pressure from '../../asset/device/water_pressure.png'
import device_water_level from '../../asset/device/water_level.png'
import device_door_lock from '../../asset/device/door_lock.png'
import device_door_lock_flag from '../../asset/device/door_lock_flag.png'
import device_plug_base from '../../asset/device/plug_base.png'
import device_face_detector from '../../asset/device/face_detector.png'
import device_camera from '../../asset/device/camera.png'
import device_box from '../../asset/device/box.svg'

import './index.less'


@connect(({ counter }) => ({
  counter
}), (dispatch) => ({
  add() {
    dispatch(add())
  },
  dec() {
    dispatch(minus())
  },
  asyncAdd() {
    dispatch(asyncAdd())
  }
}))
class Index extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      current: 0,
      tabList: [
        {
          title: '设备',
        },
        {
          title: '房间',
        },
      ],
      devices: [],
      rooms: [],
      numberOfIssueDevice: 0,
      headerBtnPosi: {
        bottom: 0,
        height: 0,
        left: 0,
        right: 0,
        top: 0,
        width: 0,
      },
      systemInfo: {
        statusBarHeight: 0
      }
    }
  }

  config = {
    navigationBarTitleText: '智慧家庭',
    navigationStyle: 'custom',
    navigationBarTextStyle: 'white', // status bar text color
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props, nextProps)
  }

  componentDidMount() {
    const devices = [
      {
        type: '智能烟感',
        typeId: 1,
        totalNum: 10,
        issueNum: 0,
      },
      {
        type: '智能温感',
        typeId: 2,
        totalNum: 10,
        issueNum: 0,
      },
      {
        type: '智能摄像头',
        typeId: 3,
        totalNum: 4,
        issueNum: 0,
      },
      {
        type: '水压仪',
        typeId: 4,
        totalNum: 10,
        issueNum: 1,
      },
      {
        type: '液位仪',
        typeId: 5,
        totalNum: 10,
        issueNum: 1,
      },
      {
        type: '智能门锁',
        typeId: 6,
        totalNum: 2,
        issueNum: 0,
      },
      {
        type: '智能门磁',
        typeId: 7,
        totalNum: 3,
        issueNum: 1,
      },
      {
        type: '智能插排',
        typeId: 8,
        totalNum: 10,
        issueNum: 2,
      },
      {
        type: '人脸识别',
        typeId: 9,
        totalNum: 2,
        issueNum: 0,
      },
    ];
    const rooms = [
      {
        name: '客厅',
        deviceTypes: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        totalNum: 10,
        issueNum: 0,
      },
      {
        name: '书房',
        deviceTypes: [1, 2, 3, 4],
        totalNum: 10,
        issueNum: 4,
      },
      {
        name: '厨房',
        deviceTypes: [5, 6, 7, 8, 9, 10],
        totalNum: 10,
        issueNum: 4,
      },
      {
        name: '车库',
        deviceTypes: [1, 3, 5, 7, 9],
        totalNum: 10,
        issueNum: 0,
      },
    ];

    Taro.getSystemInfo({
      success: res => {
        // this.globalData.systemInfo = res
        this.setState({ systemInfo: { ...res } })
      }
    })

    const posi = Taro.getMenuButtonBoundingClientRect()
    // console.log('this.globalData.headerBtnPosi', this.globalData.headerBtnPosi);
    this.setState({
      devices,
      rooms,
      numberOfIssueDevice: devices.reduce((prev,curr) => prev + curr.issueNum, 0),
      headerBtnPosi: { ...posi }
    })
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  renderFastFeatureBox = () => {
    const ffItems = [
      {
        title: '联系管家',
        icon: ff_icon_1,
        action: null,
      },
      {
        title: '异常设备',
        icon: ff_icon_2,
        action: null,
      },
      {
        title: '添加设备',
        icon: ff_icon_3,
        action: null,
      },
    ]

    const renderItem = (item, idx) => <View className='ff_item_box'>
      <Image className='ff_icon' src={item.icon} />
      <Text className='ff_title'>{item.title}</Text>
    </View>

    const { numberOfIssueDevice } = this.state;
    return (
      <View className='ff_box'>
        {
          ffItems.map((item, idx) => {
            return (
              <View className='ff_item_box'>
                {
                  idx === 1
                    ? <AtBadge className='badge' value={numberOfIssueDevice} maxValue={99}><Image className='ff_icon' src={item.icon} /></AtBadge>
                    : <Image className='ff_icon' src={item.icon} />
                }
                <Text className='ff_title'>{item.title}</Text>
              </View>
            )
          })
        }
      </View>
    )
  }

  handleClick(value) {
    this.setState({
      current: value
    })
  }

  getDeviceImage = (key) => {
    let img = null;
    switch (key) {
      case 1:
        img = device_smoke_detector;
        break;
      case 2:
        img = device_temp_detector;
        break;
      case 3:
        img = device_camera;
        break;
      case 4:
        img = device_water_pressure;
        break;
      case 5:
        img = device_water_level;
        break;
      case 6:
        img = device_door_lock;
        break;
      case 7:
        img = device_door_lock_flag;
        break;
      case 8:
        img = device_plug_base;
        break;
      case 9:
        img = device_face_detector;
        break;
      default:
        img = device_box;
    }
    return img;
  }

  renderDevices = () => {
    const { devices } = this.state
    if (!devices.length) {
      return <Text>No Device</Text>
    }

    return <View className='at_tabs_pane_box'>
      {
        devices.map((item, idx) => {
          return <View className='device_box_wrap'>
            <View className='device_box'>
              <View className='device_img_type_box'>
                <Image className='device_img' src={this.getDeviceImage(item.typeId)} />
                <Text className='device_type_text'>{item.type}</Text>
              </View>
              <View className='device_info_box'>
                <Text className='device_total_text'>{item.totalNum}台设备</Text>
                <Text className={item.issueNum > 0 ? 'device_issue_text' : 'device_no_issue_text'}>{item.issueNum}台异常</Text>
              </View>
            </View>
          </View>
        })
      }
    </View>
  }

  renderRoomDevicesList = (deviceIdList) => {
    const hasDevices = deviceIdList && deviceIdList.length
    if (!hasDevices) {
      return null
    }

    const deviceImgBoxList = deviceIdList.map((typeId, idx) => {
      const img = this.getDeviceImage(typeId)
      // console.log('deviceTypes', idx, typeId, img);
      return <Image className='device_img_small room_device_box' key={typeId} src={img} />
    })

    return <View className='device_img_list_box'>
      {deviceImgBoxList}
    </View>
  }

  renderRooms = () => {
    const { rooms } = this.state
    if (!rooms.length) {
      return <Text>No Room</Text>
    }

    return <View className='at_tabs_pane_box'>
      {
        rooms.map((item, idx) => {
          return <View className='room_box'>
            <View className='room_info_box'>
              <Text className='room_title_text'>{item.name}</Text>
              <Text className='device_total_text'>{item.totalNum}台设备</Text>
              <Text className={item.issueNum > 0 ? 'device_issue_text' : 'device_no_issue_text'}>{item.issueNum}台异常</Text>
            </View>
            {this.renderRoomDevicesList(item.deviceTypes)}
          </View>
        })
      }
    </View>
  }

  renderMyHomeBox = () => {
    const { tabList, current, systemInfo, headerBtnPosi } = this.state
    console.log(systemInfo.statusBarHeight, headerBtnPosi);
    const statusBarHeight = systemInfo && systemInfo.statusBarHeight || 0;
    const offset = headerBtnPosi.top - statusBarHeight;
    const style = {
      // backgroundColor: 'red',
      // alignItems: 'center',
      // justifyContent: 'center',
      // display: 'flex',
      height: `${headerBtnPosi.height}px`,
      marginTop: `${headerBtnPosi.top}px`
    }
    return <View className='content_box'>
      {/* <View className='title_box'> */}
      <View className='title_box' style={style}>
        <Text className='title_text'>纳智慧 · 安如斯</Text>
      </View>
      <View className='home_title_box'>
        <Text>我的智慧家庭</Text>
        <Image className='menu_arrow_right' src={arrow_right} />
      </View>
      {this.renderFastFeatureBox()}
      <View className='home_box'>
        <AtTabs className='at_tabs' current={current} tabList={tabList} onClick={this.handleClick.bind(this)}>
          <AtTabsPane className='at_tabs_pane' current={current} index={0} >
            {this.renderDevices()}
          </AtTabsPane>
          <AtTabsPane className='at_tabs_pane' current={current} index={1} >
            {this.renderRooms()}
          </AtTabsPane>
        </AtTabs>
      </View>
    </View>
  }

  render() {
    return (
      <View className='index'>
        <Image className='top_img' src={mine_top} mode='widthFix' />
        {this.renderMyHomeBox()}
      </View>
    )
  }
}

export default Index
