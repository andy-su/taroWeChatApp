import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Image } from '@tarojs/components'
import { connect } from '@tarojs/redux'

import { add, minus, asyncAdd } from '../../actions/counter'

import mine_top from '../../asset/mine_top.png'
import default_avatar from '../../asset/avatar_default.svg'
import fav_icon_1 from '../../asset/chazhao.svg'
import fav_icon_2 from '../../asset/shuben.svg'
import fav_icon_3 from '../../asset/xiaoxi.svg'
import fav_icon_4 from '../../asset/shezhi.svg'

import menu_icon_1 from '../../asset/jiaocai.svg'
import menu_icon_2 from '../../asset/qiandao.svg'
import menu_icon_3 from '../../asset/shexiangtou.svg'
import menu_icon_4 from '../../asset/shijian.svg'
import menu_icon_5 from '../../asset/tixing.svg'
import menu_icon_6 from '../../asset/tupian.svg'
import menu_icon_7 from '../../asset/xihuan.svg'
import menu_icon_8 from '../../asset/xingping.svg'

import arrow_right from '../../asset/arrow/arrow_right.svg'

import './index.less'


@connect(({ counter }) => ({
  counter
}), (dispatch) => ({
  add () {
    dispatch(add())
  },
  dec () {
    dispatch(minus())
  },
  asyncAdd () {
    dispatch(asyncAdd())
  }
}))
class Index extends Component {

    config = {
    navigationBarTitleText: '我的',
    navigationStyle: 'custom', // hide navigation bar
    navigationBarTextStyle: 'white', // status bar text color
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  renderFavoriteItems = () => {
    const favorites = [
      {
        icon: fav_icon_1,
        title: '我的订单',
        action: null,
      },
      {
        icon: fav_icon_2,
        title: '我的方案',
        action: null,
      },
      {
        icon: fav_icon_3,
        title: '消息中心',
        action: null,
      },
      {
        icon: fav_icon_4,
        title: '配置系统',
        action: null,
      },
    ]
    return favorites.map((item, idx) => {
      return <View className='fav_item_container' key={idx}>
        <Image className='fav_icon' src={item.icon} />
        <Text className='fav_title'>{item.title}</Text>
      </View>
    })
  }

  renderMenuItems = () => {
    const menus = [
      {
        icon: menu_icon_1,
        title: '我的订单1',
        action: null,
      },
      {
        icon: menu_icon_2,
        title: '我的方案2',
        action: null,
      },
      {
        icon: menu_icon_3,
        title: '消息中心',
        action: null,
      },
      {
        icon: menu_icon_4,
        title: '配置系统',
        action: null,
      },
      {
        icon: menu_icon_5,
        title: '我的订单',
        action: null,
      },
      {
        icon: menu_icon_6,
        title: '我的方案',
        action: null,
      },
      {
        icon: menu_icon_7,
        title: '消息中心',
        action: null,
      },
      {
        icon: menu_icon_8,
        title: '配置系统',
        action: null,
      },
    ]

    const othersView = menus.slice(1).map((item, idx) => {
      return <View className='menu_item_container' key={idx + 1}>
        <Image className='menu_icon' src={item.icon} />
        <Text className='menu_title'>{item.title}</Text>
      </View>
    })

    const firstItem = menus.slice(0, 1).map((item, idx) => {
      return <View className='menu_item_container_first' key={idx}>
        <Image className='menu_icon' src={item.icon} />
        <Text className='menu_title'>{item.title}</Text>
      </View>
    })
    // const itemsView = [firstItem];
    // return itemsView.concat(othersView);
    // console.log('render menu', firstItem, othersView);
    return menus.map((item, idx) => {
      return <View className={idx ? 'menu_item_container' : 'menu_item_container menu_item_container_first'} key={idx + 1}>
      <Image className='menu_icon' src={item.icon} />
      <Text className='menu_title'>{item.title}</Text>
      <Image className='menu_arrow_right' src={arrow_right} />
    </View>
    })
  }

  render () {
    return (
      <View className='index'>
        <Image className= 'top_img' src={mine_top} mode='widthFix' />
        <View className='my_info_container'>
          <Image className='avatar' src={default_avatar} />
          <View className='my_info_text'>
            <Text className='info_name'>张三丰</Text>
            <Text className='info_phone'>13712345678</Text>
          </View>
        </View>
        <View className='my_favorite_container'>
          {this.renderFavoriteItems()}
        </View>
        {/* <Text className='text22'>张三丰</Text>
        <Text className='text21'>张三丰</Text>
        <Text className='text1'>张三丰</Text>
        <Text className='text2'>张三丰</Text>
        <Text className='text3'>张三丰</Text>
        <Text className='text4'>张三丰</Text>
        <Text className='text5'>张三丰</Text>
        <Text className='text6'>张三丰</Text>
        <Text className='text7'>张三丰</Text> */}
        <View className='menu_container'>
          {this.renderMenuItems()}
        </View>
      </View>
    )
  }
}

export default Index
