import Taro, { Component } from '@tarojs/taro'
import { View, Button, Text, Image, ScrollView } from '@tarojs/components'
import { AtTabs, AtTabsPane } from 'taro-ui'
import { connect } from '@tarojs/redux'

import { add, minus, asyncAdd } from '../../actions/counter'

import './index.less'


@connect(({ counter }) => ({
  counter
}), (dispatch) => ({
  add () {
    dispatch(add())
  },
  dec () {
    dispatch(minus())
  },
  asyncAdd () {
    dispatch(asyncAdd())
  }
}))
class Discover extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      current: 0,
      imgs: [
        // 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591435649133&di=a0f31d2a6cf18bdd0addacf797f7f6e1&imgtype=0&src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20170328%2Fad239fe1964747ae9d695b8a646a9abf.gif',
        'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591436365111&di=ce639a6135005ea12c9fd86bba474a91&imgtype=0&src=http%3A%2F%2Fm.360buyimg.com%2Fpop%2Fjfs%2Ft26044%2F25%2F222669715%2F274357%2F68fb1e1e%2F5b6952b5N3620c3e7.jpg',
        'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591436482073&di=d5b5493e682835c53eb821a72856f717&imgtype=0&src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20170327%2F34379172f17c4e88a276748d252b534f_th.jpeg',
        'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3464232381,1576723388&fm=15&gp=0.jpg',
        'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591440605689&di=de4f8bbcafca7745822e87f4b3c569bc&imgtype=0&src=http%3A%2F%2Fimg30.360buyimg.com%2FpopWaterMark%2Fjfs%2Ft2548%2F316%2F1085242487%2F41562%2F64647b4e%2F568b1299N37b9bcca.jpg',
        'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591440594232&di=b63cd1e21a1e77778309ee82d920714c&imgtype=0&src=http%3A%2F%2Fpic.lvmama.com%2Fpics%2Fsuper%2F2014%2F08%2FTYGDC.jpg',
      ],
      tabList: [
        {
          title: '热门活动',
        },
        {
          title: '全屋定制',
        },
        {
          title: '经典案例',
        },
        {
          title: '百科',
        },
      ],
      colors: [
        '#ff7214',
        '#23a800',
        '#da2020',
        '#00bdbd',
      ],
      hotEvents: [],
      roomCustomization: [],
      classicCases: [],
      wiki: [],
    }
  }

  config = {
    navigationBarTitleText: '发现 · 智慧生活',
    // navigationStyle: 'custom',
    // navigationBarTextStyle: 'white', // status bar text color
  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentDidMount() {
    const { imgs, current } = this.state;
    this.setState({
      bannerImg: imgs[current],
    })
    this.handleClick(0);
    this.buildFakeData();
  }

  buildFakeData = () => {
    const { hotEvents, roomCustomization, classicCases, wiki, imgs } = this.state;
    const titles = [
      'AI摄像机系列',
      '高清网络摄像机',
      '专用型网络摄像机',
      'Smart IPC',
      '专业智能分析网络摄像机',
      'Smart高清网络球型摄像机',
      'E系列高清网络球型摄像机',
      'PTZ系列摄像机',
      '全景系列',
      '智能交通球型摄像机',
    ]
    const subtitles = [
      '原来海康威视在大兴机场做了这些事',
      '海康威视助力深圳机场打造视频监控联网共享平台',
      '海康威视助力广州出租屋智能化综合防控体系建设',
      '电动汽车充电站可视化管理怎么做，看G20试点项目',
      '魔方公寓全国门店联网监控项目',
      '大兴安岭某森林防火视频监控系统试点项目',
      '魔方公寓全国门店联网监控项目',
      '助力华东万和城打造智能商业体，成就巅峰之作',
      '打造杭城顶级商贸旅游综合体',
      '综合安防助力智慧工厂',
    ]
    for (let index = 0; index < 10; index++) {
      const radm = Math.random();
      const element = {
        title: titles[index % titles.length],
        subtitle: subtitles[Math.floor(radm * 10)],
        read: Math.floor(radm * 100),
        like: Math.floor(radm * 10),
        img: imgs[index % imgs.length],
        style: radm > 0.5,
      }
      hotEvents.push(element);
      classicCases.push(element);
    }
    for (let index = 0; index < 10; index++) {
      const radm = Math.random();
      const element = {
        title: titles[Math.floor(radm * 10)],
        subtitle: subtitles[index % subtitles.length],
        read: Math.floor(radm * 100),
        like: Math.floor(radm * 10),
        img: imgs[index % imgs.length],
        style: radm > 0.5,
      }
      roomCustomization.push(element);
      wiki.push(element);
    }
    this.setState({
      hotEvents, roomCustomization, classicCases, wiki
    })
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  setStatusBarColor = (params) => {
    Taro.setNavigationBarColor(params).then((res) => {
      console.log('setStatusBarTextColor', res);
    })
  }

  handleClick (value) {
    // let bannerImg = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1591435649133&di=a0f31d2a6cf18bdd0addacf797f7f6e1&imgtype=0&src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20170328%2Fad239fe1964747ae9d695b8a646a9abf.gif';
    const { imgs, colors } = this.state;
    this.setState({
      current: value,
      bannerImg: imgs[value],
    })
    const params = {
      frontColor: '#ffffff',
      backgroundColor: colors[value],
      animation: {
        duration: 500,
        timingFunc: 'easeOut',
      }
    }
    this.setStatusBarColor(params);
  }

  renderTabContentItems = (key) => {
    const { hotEvents, roomCustomization, classicCases, wiki } = this.state;
    let list = [];
    switch (key) {
      case 0: {
        list = hotEvents
      }
        break;
      case 1: {
        list = roomCustomization
      }
        break;
      case 2: {
        list = classicCases
      }
        break;
      default: {
        list = wiki
      }
    }
    return list.map((item, idx) => {
      return item.style
        ? (
          <View className='item_full_img_box'>
            <Image className='item_full_img' src={item.img} />
            <View className='item_full_title_box'>
              <Text className='item_full_img_title_text'>{item.title}</Text>
              <Text className='item_full_img_subtitle_text'>{item.subtitle}</Text>
            </View>
          </View>
        )
        : (
          <View className='item_box'>
            <Image className='item_img' src={item.img} />
            <View className='item_title_box'>
              <Text className='item_title_text'>{`${item.title}: ${item.subtitle}`}</Text>
              <Text className='item_info_text'>{`${item.read}阅读 ${item.like}喜欢`}</Text>
            </View>
          </View>
        )
    })
  }

  renderAtTabsPane = () => {
    const { tabList, current } = this.state
    return tabList.map((item, idx) => {
      return (
        <AtTabsPane className='at_tabs_pane' current={current} index={idx} >
          <ScrollView className='at_tabs_pane_box'>
            {this.renderTabContentItems(idx)}
          </ScrollView>
        </AtTabsPane>
      )
    })
  }

  render() {
    const { tabList, bannerImg, current } = this.state
    return (
      <View className='index'>
        {/* <Image className='banner_img' src={bannerImg} />
        <View className='banner_box' /> */}
        <AtTabs className='at_tabs' current={current} tabList={tabList} onClick={this.handleClick.bind(this)}>
          {this.renderAtTabsPane()}
        </AtTabs>
      </View>
    )
  }
}

export default Discover
