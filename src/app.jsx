import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/redux'

import Index from './pages/index'

import configStore from './store'
import 'taro-ui/dist/style/index.scss'

import './app.less'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

const store = configStore()

class App extends Component {

  config = {
    pages: [
      'pages/index/index',
      'pages/discover/index',
      'pages/shopping/index',
      'pages/mine/index',
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      list: [
        {
          pagePath: 'pages/index/index',
          text: '智家',
          iconPath: './asset/dianpu_gray.png',
          selectedIconPath: './asset/dianpu.png',
        },
        {
          pagePath: 'pages/discover/index',
          text: '发现',
          iconPath: './asset/jiangpai_gray.png',
          selectedIconPath: './asset/jiangpai.png',
        },
        {
          pagePath: 'pages/shopping/index',
          text: '小铺',
          iconPath: './asset/dingwei_gray.png',
          selectedIconPath: './asset/dingwei.png',
        },
        {
          pagePath: 'pages/mine/index',
          text: '我的',
          iconPath: './asset/huiyuan_gray.png',
          selectedIconPath: './asset/huiyuan.png',
        }
      ],
      color: '#8f8f8f',
      selectedColor: '#ff7214',
      backgroundColor: '#fff',
    }
  }

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
